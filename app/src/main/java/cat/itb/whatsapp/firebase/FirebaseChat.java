package cat.itb.whatsapp.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import cat.itb.whatsapp.models.ChatModelFirebase;


public class FirebaseChat {
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    public static FirebaseRecyclerOptions<ChatModelFirebase> firebaseRecyclerOptions;
    public static StorageReference storageReference;


    public static void inicializeDatabase(String telephone) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        Log.e("telefono", telephone);
        Query query = firebaseDatabase.getReference("chats")
                .orderByChild("telephone1").equalTo(telephone);
//                .orderByChild("telephone1")
//                .startAt(String.valueOf(telephone.charAt(0)))
//                .endAt(String.valueOf(telephone.charAt(telephone.length()-1)));


        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<ChatModelFirebase>()
                .setQuery(query, ChatModelFirebase.class)
                .build();

        databaseReference = firebaseDatabase.getReference("chats");


        //storageReference = FirebaseStorage.getInstance().getReference().child("chats"); //carpeta del storage "chats"
    }

    public static void insert(ChatModelFirebase c) {
        String key = databaseReference.push().getKey();
        c.setId(key);
        databaseReference.child(key).setValue(c);
    }

    public static List<ChatModelFirebase> getChatsByPhoneNumber(String phone) {
        List<ChatModelFirebase> chatList = new ArrayList<>();
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    ChatModelFirebase c = postSnapshot.getValue(ChatModelFirebase.class);
                    if (c.getTelephone1().equalsIgnoreCase(phone)) {
                        Log.e("getchats"," EN FIRE BASE CHAT: "+ "este chat lo añadimos a la lista");

                        //Log.e("getchats"," EN FIRE BASE CHAT: "+ c.getTelephone1());
                        chatList.add(c);
                        Log.e("getchats"," EN FIRE BASE CHAT: list.size()= "+ chatList.size());

                    } else {
                        Log.e("getchats"," EN FIRE BASE CHAT: "+ "este chat NO lo añadimos a la lista");

                        //Log.e("getchats"," EN FIRE BASE CHAT: "+ c.getTelephone1());
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
        return chatList;
    }

    public static void update(ChatModelFirebase m) {
        databaseReference.child(m.getId()).setValue(
                new ChatModelFirebase(m.getId(), m.getPhoto(), m.getTelephone1(), m.getTelephone2(), m.getLastMessage(), m.getLastMessageTime(), m.isRead()));
    }
}
