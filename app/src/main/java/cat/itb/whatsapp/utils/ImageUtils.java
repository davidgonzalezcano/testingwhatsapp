package cat.itb.whatsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import id.zelory.compressor.Compressor;

public class ImageUtils {

    public static void recortarImagen(Uri imageUri, Activity activity) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 480)
                .setAspectRatio(2, 1).start(activity);
    }

    public static byte[] comprimirImagen(Context context, File fileUrl) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap thumbBitmap = null;
        if (fileUrl != null) {
            try {
                thumbBitmap = new Compressor(context)
                        .setMaxHeight(125)
                        .setMaxWidth(125)
                        .setQuality(50)
                        .compressToBitmap(fileUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        }
        return byteArrayOutputStream.toByteArray();
    }
}
