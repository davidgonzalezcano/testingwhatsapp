package cat.itb.whatsapp.utils;

import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;

public class NoUnderlineSpan {

    public NoUnderlineSpan() {
    }

    public Spannable noUnderlineSpan(String text) {
        Spannable s = (Spannable) Html.fromHtml(text);
        for (
                URLSpan u: s.getSpans(0, s.length(), URLSpan.class)) {
            s.setSpan(new UnderlineSpan() {
                public void updateDrawState(TextPaint tp) {
                    tp.setUnderlineText(false);
                }
            }, s.getSpanStart(u), s.getSpanEnd(u), 0);
        }
        return s;
    }
}
