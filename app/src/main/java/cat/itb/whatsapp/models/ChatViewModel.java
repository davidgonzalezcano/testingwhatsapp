package cat.itb.whatsapp.models;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class ChatViewModel extends ViewModel {

    public List<Chat> chats = new ArrayList<Chat>();

    String[] names = {"Pedro","Maria","Juan","Berta","Marcos","Sara","David","Jordi","Pedro","Maria","Juan","Berta","Marcos","Sara","David","Jordi"};
    String[] lastMessages = {"Hola?","Buenas! Cuanto tiempo...","Adios!","Como vas con la faena? ...","Has acabado la faena?","Tenemos que avanzar!","Okey",
            "Si señor!","Jejeje","Hahahaha","Buenos dias","Por aqui todo bien ...","Por si las moscas","Vamos tarde, tenemos que espabilar","Todo bien","Okis"};

    public ChatViewModel() {
        for (int i = 0; i <names.length; i++) {
            Chat chat = new Chat(String.valueOf(i), "photo:"+i, names[i], lastMessages[i], ("12:5"+i%10), i==0 || i==1 | i==2 | i==3);
            chats.add(chat);
        }
    }

}
