package cat.itb.whatsapp.models;

public class Llamada {

    String nameUser, tipoLlamada, fechaLlamada;

    public Llamada(String nameUser, String tipoLlamada, String fechaLlamada) {
        this.nameUser = nameUser;
        this.tipoLlamada = tipoLlamada;
        this.fechaLlamada = fechaLlamada;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTipoLlamada() {
        return tipoLlamada;
    }

    public void setTipoLlamada(String tipoLlamada) {
        this.tipoLlamada = tipoLlamada;
    }

    public String getFechaLlamada() {
        return fechaLlamada;
    }

    public void setFechaLlamada(String fechaLlamada) {
        this.fechaLlamada = fechaLlamada;
    }
}
