package cat.itb.whatsapp.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ChatModelFirestore implements Serializable {
    private String id;
    private String photo;
    private ArrayList<String> telephones = new ArrayList<>();
    private String message;
    private String messageTime;
    //private boolean isRead;
    //private int countMessages;
    private HashMap<String, Integer> conuntMessagesHashMap = new HashMap<>();
    private HashMap<String, Boolean> isReadMessagesHashMap = new HashMap<>();
    private HashMap<String, Integer> lastMessageHashMap = new HashMap<>();

    public ChatModelFirestore() { }

    public ChatModelFirestore(String id,
                              String photo,
                              ArrayList<String> telephones,
                              String message,
                              String messageTime) {
        this.id = id;
        this.photo = photo;
        this.telephones = telephones;
        this.message = message;
        this.messageTime = messageTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ArrayList<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(ArrayList<String> telephones) {
        this.telephones = telephones;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public HashMap<String, Integer> getConuntMessagesHashMap() {
        return conuntMessagesHashMap;
    }

    public void setConuntMessagesHashMap(HashMap<String, Integer> conuntMessages) {
        this.conuntMessagesHashMap = conuntMessages;
    }

    public HashMap<String, Boolean> getIsReadMessagesHashMap() {
        return isReadMessagesHashMap;
    }

    public void setIsReadMessagesHashMap(HashMap<String, Boolean> isReadMessages) {
        this.isReadMessagesHashMap = isReadMessages;
    }

    public HashMap<String, Integer> getLastMessageHashMap() {
        return lastMessageHashMap;
    }

    public void setLastMessageHashMap(HashMap<String, Integer> lastMessageHashMap) {
        this.lastMessageHashMap = lastMessageHashMap;
    }

}
