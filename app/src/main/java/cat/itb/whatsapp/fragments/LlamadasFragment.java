package cat.itb.whatsapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.adapters.LlamadasAdapter;
import cat.itb.whatsapp.models.Llamada;
import cat.itb.whatsapp.models.LlamadaViewModel;

public class LlamadasFragment extends Fragment {

    private LlamadaViewModel llamadaViewModel = null;
    private ListView listView;
    private LlamadasAdapter llamadasAdapter;

    ArrayList<Llamada> llamadaArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] names = {"Pedro Sanchez","Maria Garcia","Juan Pedro","Berta Romero","Marcos Montoro","Sara Diaz","David Conte","Jordi Cidoncha","Pedro Juan","Maria Martinez","Juan Roman","Berta Benito","Marcos Benito","Sara Benito","David","Jordi"};

        String[] estadoLlamadas = {"Entrante", "Saliente", "Perdida"};

        for (int i = 0; i < names.length; i++) {
            Llamada llamada = new Llamada(names[i], estadoLlamadas[i%3], 1+(i%30)+"/02/21");
            llamadaArrayList.add(llamada);
        }


        llamadaViewModel = new ViewModelProvider(requireActivity()).get(LlamadaViewModel.class);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_llamadas, container, false);

        listView = view.findViewById(R.id.listViewLlamadas);
        llamadasAdapter = new LlamadasAdapter(getContext(), R.layout.item_llamada, llamadaArrayList);

        listView.setAdapter(llamadasAdapter);
        listView.setDivider(null);

        return view;

    }
}