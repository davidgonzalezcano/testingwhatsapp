package cat.itb.whatsapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.activities.MensajesChatActivity;
import cat.itb.whatsapp.firebase.FirestoreChat;
import cat.itb.whatsapp.models.ChatModelFirestore;
import cat.itb.whatsapp.models.User;

public class FirestoreChatsAdapter extends FirestoreRecyclerAdapter
        <ChatModelFirestore, FirestoreChatsAdapter.ChatHolderFirestore> {

    public Context context;

    public static User user = null;

    public static ChatModelFirestore chatClickedInAdapter;


    public View view;


    public FirestoreChatsAdapter(@NonNull FirestoreRecyclerOptions<ChatModelFirestore> options, User user) {
        super(options);
        FirestoreChatsAdapter.user = user;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onBindViewHolder(@NonNull ChatHolderFirestore holder,
                                    int position, @NonNull ChatModelFirestore model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public ChatHolderFirestore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_chat_list, parent, false);
        this.view = v;
        return new ChatHolderFirestore(v);
    }

    public class ChatHolderFirestore extends RecyclerView.ViewHolder {

//        private final ShapeableImageView shapeableImageViewIsRead;
        private final ImageView shapeableImageViewIsRead;

        private final TextView textViewName;
        private final TextView textViewLastMessage;
        private TextView textViewTime;

        private ImageView imageViewContact;

        public ChatModelFirestore chat;

        public ChatHolderFirestore(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewLastMessage = itemView.findViewById(R.id.textViewTipoLlamada);
            textViewTime = itemView.findViewById(R.id.textViewTimeLlamada);
            shapeableImageViewIsRead = itemView.findViewById(R.id.imageShapableIsIcInfo);
            imageViewContact = itemView.findViewById(R.id.imageViewContact);


            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, MensajesChatActivity.class);
                intent.putExtra("CREATE", 0);

                chatClickedInAdapter = chat;

                context.startActivity(intent);
            });
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void bind(ChatModelFirestore chat) {

            Log.e("hello", "in firestore chats adapter");
            this.chat = chat;

            // chat normal
            if (chat.getTelephones().size()==2) {

                textViewName.setText(chat.getTelephones().get(0));
                if (FirestoreChatsAdapter.user.getTelefono().equals(chat.getTelephones().get(0)))
                    textViewName.setText(chat.getTelephones().get(1));
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Group: ");
                for (String tel: chat.getTelephones()) {
                    if (!tel.equals(user.getTelefono())) {
                        stringBuilder.append(tel).append(" ");
                    }
                }

                textViewName.setText(stringBuilder.toString());
            }


            textViewLastMessage.setText(chat.getMessage());

            textViewTime.setText(getTime(chat.getMessageTime()));

            //TODO ING
            getTime(chat.getMessageTime());

            //CONTACT IMAGE
            try {
                Picasso.with(imageViewContact.getContext())
                        .load(chat.getPhoto())
                        .into(imageViewContact);
            } catch (Exception e) {
                Log.e("exception", e.toString());
            }

            //TICK IMAGE
            if (chat.getIsReadMessagesHashMap().get(user.getTelefono())) {
                shapeableImageViewIsRead.setVisibility(View.INVISIBLE);
            } else {
                shapeableImageViewIsRead.setVisibility(View.VISIBLE);
                //show 1, 2 , 3 or +3
                switch (chat.getConuntMessagesHashMap().get(user.getTelefono())) {
                    case 0:
                        shapeableImageViewIsRead.setVisibility(View.INVISIBLE);
                        break;
                    case 1:
                        shapeableImageViewIsRead.setImageDrawable(
                                context.getDrawable(R.drawable.ic_one_message_round));
                        break;
                    default:
                        shapeableImageViewIsRead.setImageDrawable(
                                context.getDrawable(R.drawable.ic_one_message_more_foreground));
                        break;
                }

            }

        }
    }

    @SuppressLint("WrongConstant")
    public void showUndoSnackbar(int position) {

        Snackbar snackbar = Snackbar.make(this.view, "Esta seguro?", Snackbar.LENGTH_LONG);
        snackbar.setAction("Si", v -> deleteItem(position));

        snackbar.show();
    }


    public void deleteItem(int position) {
        ChatModelFirestore chat= getItem(position);
        FirestoreChat.delete(chat.getId());
        notifyItemRemoved(position);
        //showUndoSnackbar(position);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getTime(String date) {
        String[] time = date.split(" ");

        Log.e("444", time[3]);
        return time[3];
    }
}
