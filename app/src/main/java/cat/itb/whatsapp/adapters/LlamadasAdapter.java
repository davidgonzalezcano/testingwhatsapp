package cat.itb.whatsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.models.Llamada;

public class LlamadasAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Llamada> data;

    public LlamadasAdapter(Context context, int layout, ArrayList<Llamada> data) {
        this.context = context;
        this.layout = layout;
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //ViewHolder Pattern
        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);

            holder = new ViewHolder();


            holder.nameTextView = convertView.findViewById(R.id.textViewNameLlamada);
            holder.typeTextView = convertView.findViewById(R.id.textViewTipoLlamada);
            holder.timeTextView = convertView.findViewById(R.id.textViewTimeLlamada);

            //Guardem el holder a la vista
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Portem el valor actual depenent de la posicio
        Llamada currentObject = (Llamada) getItem(position);

        //Referenciem l'element modificar i l'omplim
        holder.nameTextView.setText(currentObject.getNameUser());
        holder.typeTextView.setText(currentObject.getTipoLlamada());
        holder.timeTextView.setText(currentObject.getFechaLlamada());

        return convertView;
    }
    static class ViewHolder{
        private TextView nameTextView;
        private TextView typeTextView;
        private TextView timeTextView;
    }

}
