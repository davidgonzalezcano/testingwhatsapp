package cat.itb.whatsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.models.FirestoreModeloMensaje;
import cat.itb.whatsapp.models.User;

public class FirestoreMensajeAdapter extends FirestoreRecyclerAdapter
        <FirestoreModeloMensaje, FirestoreMensajeAdapter.MensajeHolderFirestore> {

    public Context context;

    public static User user = null;

    public FirestoreMensajeAdapter(@NonNull FirestoreRecyclerOptions<FirestoreModeloMensaje> options, User user) {
        super(options);
        FirestoreMensajeAdapter.user = user;
    }

    @Override
    protected void onBindViewHolder(@NonNull MensajeHolderFirestore mensajeHolderFirestore,
                                    int i, @NonNull FirestoreModeloMensaje firestoreModeloMensaje) {
        mensajeHolderFirestore.bind(firestoreModeloMensaje);
    }

    @NonNull
    @Override
    public MensajeHolderFirestore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mensaje, parent, false);
        return new MensajeHolderFirestore(v);
    }

    public class MensajeHolderFirestore extends RecyclerView.ViewHolder {
        private TextView textViewMensaje;
        private TextView fecha;

        public MensajeHolderFirestore(@NonNull View itemView) {
            super(itemView);
            textViewMensaje = itemView.findViewById(R.id.mensaje_texto);
        }

        public void bind(FirestoreModeloMensaje firestoreModeloMensaje) {

            // TODO: setear el estilo en funcion del emisor/receptor y el telefono de la variable user
            textViewMensaje.setText(firestoreModeloMensaje.getMensaje());
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
