package cat.itb.whatsapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.activities.MensajesChatActivity;
import cat.itb.whatsapp.firebase.FirebaseMensaje;
import cat.itb.whatsapp.models.Mensaje;

public class MensajeAdapterFirebase extends FirebaseRecyclerAdapter<Mensaje, MensajeAdapterFirebase.MensajeHolder> {

    public Context context;
    private Mensaje mensaje;

    public MensajeAdapterFirebase(@NonNull FirebaseRecyclerOptions<Mensaje> options) {
        super(options);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onBindViewHolder(@NonNull MensajeHolder holder, int position, @NonNull Mensaje model) {
        this.mensaje = model;
        holder.bind(model);

    }

    @NonNull
    @Override
    public MensajeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mensaje, parent, false);
        return new MensajeHolder(v);
    }

    public class MensajeHolder extends RecyclerView.ViewHolder {

        private TextView textViewMensaje;
        private LinearLayout linearLayoutBoxMEssage;
        private TextView fecha;

        public MensajeHolder(@NonNull View itemView) {
            super(itemView);
            textViewMensaje = itemView.findViewById(R.id.mensaje_texto);
            linearLayoutBoxMEssage = itemView.findViewById(R.id.text_box_message);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.e("123", mensaje.getChatId());
                    showUndoSnackbar(v, mensaje.getChatId());
                    return true;
                }
            });

        }


        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("RtlHardcoded")
        public void bind(Mensaje m) {

            if (MensajesChatActivity.user.getTelefono().equals(m.getTelefonoEmisor())) {
                //textViewMensaje.setTextColor(context.getResources().getColor(R.color.green));
                textViewMensaje.setBackgroundColor(context.getResources().getColor(R.color.my_message));
                //textViewMensaje.setGravity();
                linearLayoutBoxMEssage.setGravity(Gravity.RIGHT);
                textViewMensaje.setBackground(context.getDrawable(R.drawable.bg_my_text_message));


            } else {
                //textViewMensaje.setTextColor(context.getResources().getColor(R.color.design_default_color_error));
                textViewMensaje.setBackgroundColor(context.getResources().getColor(R.color.you_message));
                linearLayoutBoxMEssage.setGravity(Gravity.LEFT);
                textViewMensaje.setBackground(context.getDrawable(R.drawable.bg_your_text_message));

                //textViewMensaje.setGravity(Gravity.LEFT);
            }
            textViewMensaje.setText(m.getMensaje());
        }

    }

    @SuppressLint("WrongConstant")
    public void showUndoSnackbar(View view, String id) {

        Snackbar snackbar = Snackbar.make(view, "Vas a borrar el mensaje, estas seguro?", Snackbar.LENGTH_LONG);
        snackbar.setAction("Si", v -> FirebaseMensaje.delete(mensaje.getId()));
        snackbar.show();

    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
