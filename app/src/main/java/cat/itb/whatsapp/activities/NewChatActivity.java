package cat.itb.whatsapp.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.adapters.ContactAdapter;
import cat.itb.whatsapp.models.Contact;
import cat.itb.whatsapp.models.ContactViewModel;

public class NewChatActivity extends AppCompatActivity {

    private ShapeableImageView backButton;
    private RecyclerView rvContats;

    ContactViewModel contactViewModel = null;
    private ContactAdapter contactAdapter;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);

        backButton = findViewById(R.id.btn_back);

        rvContats = findViewById(R.id.rv_contacts);

        rvContats.setLayoutManager(new LinearLayoutManager(this));

        contactViewModel = new ContactViewModel();

        checkPermission();

        backButton.setOnClickListener(v -> onBackPressed());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void checkPermission(){
        //TODO 2
        if (Build.VERSION.SDK_INT> Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions( new String[]{Manifest.permission.READ_CONTACTS},1);
        }else {
            getContacts();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getContacts() {
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                , null, null, null);

        while (cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber= cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            Contact contact = new Contact(name,phoneNumber);

            contactViewModel.add(contact);
        }

        contactAdapter = new ContactAdapter(ContactViewModel.getContacts(), this);

        rvContats.setAdapter(contactAdapter);

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //TODO 2
        if (requestCode ==1 && grantResults.length>0 && grantResults [0] == PackageManager.PERMISSION_GRANTED){
            getContacts();
        }else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            Log.e("TAG-READ_CONTACTS","Permission Denied");
            checkPermission();
        }
    }
}
