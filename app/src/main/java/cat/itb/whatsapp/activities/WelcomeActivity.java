package cat.itb.whatsapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import cat.itb.whatsapp.R;
import cat.itb.whatsapp.firebase.FirebaseAuthUser;
import cat.itb.whatsapp.utils.NoUnderlineSpan;

public class WelcomeActivity extends AppCompatActivity {

    private TextView accept_continue_btn;
    private TextView information;
    private String text;
    private NoUnderlineSpan noUnderlineSpan;

    private FirebaseAuthUser firebaseAuthUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        information = findViewById(R.id.information);
        accept_continue_btn = findViewById(R.id.accept_continue);

        firebaseAuthUser = new FirebaseAuthUser();

        setFields();

        accept_continue_btn.setOnClickListener(v -> sendUserToRegisterActivity(v));
    }

    public void setFields() {
        String information1 = getResources().getString(R.string.information1);
        String privacyLink = getResources().getString(R.string.privacyLink);
        String information2 = getResources().getString(R.string.information2);
        String conditionsLink = getResources().getString(R.string.conditionsLink);

        text = "" + information1 +
                "<a href='https://www.whatsapp.com/legal/privacy-policy-eea?lg=es&lc=ES'> " + privacyLink + ". </a> " +
                information2 +
                "<a href='https://www.whatsapp.com/legal/terms-of-service-eea?lg=es&lc=ES'> " + conditionsLink + ".";

        noUnderlineSpan = new NoUnderlineSpan();
        Spannable s = noUnderlineSpan.noUnderlineSpan(text);
        information.setText(s);
        information.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void sendUserToRegisterActivity(View view) {

        Intent registerIntent = new Intent(WelcomeActivity.this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    public void sendUserToMainActivity() {

        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);

        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e("bundle","onStart");

        if (firebaseAuthUser.getCurrentUser() != null) {
            sendUserToMainActivity();
        }
    }
}